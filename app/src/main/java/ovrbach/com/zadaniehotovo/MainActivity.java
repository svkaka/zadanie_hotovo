package ovrbach.com.zadaniehotovo;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;


import java.util.ArrayList;

import ovrbach.com.zadaniehotovo.rest.Api;
import ovrbach.com.zadaniehotovo.rest.Article;
import ovrbach.com.zadaniehotovo.rest.News;
import ovrbach.com.zadaniehotovo.rest.Trieda;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;



public class MainActivity extends AppCompatActivity implements Callback<News>, View.OnClickListener {
   // private News news;

    public static String ALT="http://s3-eu-west-1.amazonaws.com/hotovo.org-eu/public/stream/mobile/devtest/feed_alt.json";
    public static String NEALT="http://s3-eu-west-1.amazonaws.com/hotovo.org-eu/public/stream/mobile/devtest/feed.json";
    private Retrofit retrofit;
private RecyclerView recyclerView;
    private boolean isAlt=false;
    private Button btAlt,btNAlt;
    private Api news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.main_list);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        btAlt= (Button) findViewById(R.id.main_alt);
        btNAlt= (Button) findViewById(R.id.main_nealt);

        btAlt.setOnClickListener(this);
        btNAlt.setOnClickListener(this);

        Gson gsonBuilder=new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();

      // try {
           retrofit = new Retrofit.Builder()
                   .baseUrl(NEALT)
                   .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                   .build();

           news = retrofit.create(Api.class);

           makeRequest(isAlt);
      // }catch (IllegalStateException e){}
    }

    void makeRequest(boolean whichOne){

        if (whichOne) {
            news.news(ALT).enqueue(this);
        }else{
            news.news(NEALT).enqueue(this);
        }
        isAlt=!isAlt;
    }

    @Override
    public void onResponse(Response<News> response, Retrofit retrofit) {
//response.raw().
        //JsonArray ja=new JsonArray(response.raw().toString());
        ArrayList<Article> articles=new ArrayList<>();
        for (Trieda t:response.body().getFeed().getItem()){
            articles.add(t.getArticle());
        }
        RecyclerView.Adapter adpater = new Adapter(articles);
        recyclerView.setAdapter(adpater);

        System.out.println(response.body().getFeed());
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onClick(View v) {
makeRequest(isAlt);
    }
}
