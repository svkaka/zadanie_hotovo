package ovrbach.com.zadaniehotovo;
//:*

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ovrbach.com.zadaniehotovo.rest.Article;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private ArrayList<Article> data = new ArrayList<>();

    public Adapter(ArrayList<Article> data) {
        this.data = data;
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Article Article = data.get(position);

        SimpleDateFormat sp = new SimpleDateFormat("dd. MM. yyyy");
        String date = sp.format(Article.getPubDate());
        try {
            Glide.with(holder.itemView.getContext())
                    .load(Article.getImage().getUrl())

                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.img);
            holder.img.setContentDescription(Article.getImage().getImageTitle());
        } catch (NullPointerException e) {
            holder.img.setVisibility(View.GONE);
        }
        holder.title.setText(Article.getTitle());

        holder.zing.setText(Article.getContent());
        holder.date.setText(date);
        holder.text.setText(Article.getContent());
        if (Article.isActive()) {
            holder.text.setVisibility(View.VISIBLE);
        } else {
            holder.text.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Article.isActive()) {
                    Article.setActive(!Article.isActive());
                    holder.text.setVisibility(View.VISIBLE);
                  //  holder.img.set
                } else {
                    holder.text.setVisibility(View.GONE);
                    holder.text.setText(Article.getContent());
                }

            }
        });
    }

    private void toggle() {

    }

    @Override
    public int getItemCount() {
//    System.out.println(data.size()+"DATATA SIZE");
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // SummaryItemBinding binding;
        // private final Context context;

        private View strip;
        private ImageView img;
        private TextView title, text, zing, date;

        ViewHolder(View itemView) {
            super(itemView);

            img = (ImageView) itemView.findViewById(R.id.item_img);
            title = (TextView) itemView.findViewById(R.id.item_title);
            text = (TextView) itemView.findViewById(R.id.item_text);
            zing = (TextView) itemView.findViewById(R.id.item_itro);
            date = (TextView) itemView.findViewById(R.id.item_date);

        }


    }
}



