package ovrbach.com.zadaniehotovo.rest;
//:*

public class Author {
    Long id;
    Name name;
    String email;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    class Name{
        String first;
        String last;
    }
}
