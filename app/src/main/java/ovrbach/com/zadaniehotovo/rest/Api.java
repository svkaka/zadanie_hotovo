package ovrbach.com.zadaniehotovo.rest;
//:*

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Url;

public interface Api  {
    @GET
    Call<News> news(@Url String url);
}
