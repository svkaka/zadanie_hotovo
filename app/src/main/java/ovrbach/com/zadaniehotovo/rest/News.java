package ovrbach.com.zadaniehotovo.rest;
//:*

public class News {
    private String description;
    private Feed feed;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    @Override
    public String toString() {
        return "News{" +
                "description='" + description + '\'' +
                ", feed=" + feed +
                '}';
    }
}
