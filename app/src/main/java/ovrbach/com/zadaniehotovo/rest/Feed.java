package ovrbach.com.zadaniehotovo.rest;
//:*

import java.util.ArrayList;
import java.util.Date;

public class Feed {
    private String title;
    private String language;
    private Date lastBuildDate;
private ArrayList<Trieda> item;

    @Override
    public String toString() {
        return "Feed{" +
                "title='" + title + '\'' +
                ", language='" + language + '\'' +
               // ", lastBuildDate=" + lastBuildDate +
                ", item=" + item +
                '}';
    }

    public ArrayList<Trieda> getItem() {
        return item;
    }

    public void setItem(ArrayList<Trieda> item) {
        this.item = item;
    }
}
