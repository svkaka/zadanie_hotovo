package ovrbach.com.zadaniehotovo.rest;
//:*

import java.util.ArrayList;
import java.util.Date;

public class Article {


    private Long id;
    private int site;
    private int zone;
    private Date pubDate;
    private Date lastUpdated;
    private String url;
    private String twitter;
    private image image;
    private String title;
    private String introduction;
    private String content;
    //todo s takym som sa stretol prvykrat
  //  private ArrayList<Author> authors;
  //  private ArrayList<RelatedStrories> relatedStories;

 private boolean isActive=false;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSite() {
        return site;
    }

    public void setSite(int site) {
        this.site = site;
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public ovrbach.com.zadaniehotovo.rest.image getImage() {
        return image;
    }

    public void setImage(ovrbach.com.zadaniehotovo.rest.image image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

  /*  public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }
*/
    /*public ArrayList<RelatedStrories> getRelatedStories() {
        return relatedStories;
    }

    public void setRelatedStories(ArrayList<RelatedStrories> relatedStories) {
        this.relatedStories = relatedStories;
    }
*/
    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
          isActive = active;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", site=" + site +
                ", zone=" + zone +
                ", pubDate=" + pubDate +
                ", lastUpdated=" + lastUpdated +
                ", url='" + url + '\'' +
                ", twitter='" + twitter + '\'' +
                ", image=" + image +
                ", title='" + title + '\'' +
                ", introduction='" + introduction + '\'' +
                ", content='" + content + '\'' +
                //", authors=" + authors +
               // ", relatedStories=" + relatedStories +
                //", isActive=" + isActive +
                '}';
    }
}
